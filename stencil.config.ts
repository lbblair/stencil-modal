import { sass } from '@stencil/sass';
import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'modalpoc',
  outputTargets:[
    {
      type: 'dist'
    },
    {
      type: 'www',
      serviceWorker: null
    }
  ],
  plugins: [
    sass()
  ]
};
