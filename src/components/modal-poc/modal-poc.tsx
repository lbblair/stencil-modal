import { Component, Prop, Method, State, Element } from "@stencil/core";

@Component({
  tag: "modal-poc",
  styleUrl: "modal-poc.scss",
  shadow: true
})
export class ModalPoc {

  examples = ["State Example 1", "State Example 2"]

  @Element() modalEl: HTMLElement;

  @Prop() public heading: string;
  @Prop({
    mutable: true,
    reflectToAttr: true
  })
  public visible: boolean;

  @State() showOptions = false;

  @Method() open(): void {
    this.visible = true;
  }

  @Method() close(): void {
    this.visible = false;
    this.showOptions = false;
  }

  private handleCancelClick = () => {
    this.visible = false;
  };

  private showOptionsHandler = () => {
      this.showOptions = !this.showOptions;
  }


  public render(): JSX.Element {
    let options = null;
    if (this.showOptions) {
      options = (
        this.examples.map(exp => (
          <p>{exp}</p>
        ))
      );
    }
    return (
      <div class={this.visible ? "wrapper visible" : "wrapper"}>
        <div class="modal">
          <h2 class="modal__title">{this.heading}</h2>
          <div class="modal__content">
            <slot name="content"></slot>
            <span onClick={this.showOptionsHandler}>{this.showOptions ? "Hide" : "Show"} Options</span>
            {options}
          </div>
          <div class="modal__footer">
            <slot name="footer"></slot>
          </div>
          <button type="button" class="modal__close" onClick={this.handleCancelClick}>X</button>
        </div>
      </div>
    );
  }
}
