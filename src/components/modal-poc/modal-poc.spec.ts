import { TestWindow } from '@stencil/core/testing';
import { ModalPoc } from './modal-poc';

describe('modal-poc', () => {
  it('should build', () => {
    expect(new ModalPoc()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLModalPocElement;
    let testWindow: TestWindow;

    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [ModalPoc],
        html: '<modal-poc></modal-poc>'
      });
    });

    it('should work without parameters', () => {
      expect(element.heading).toBeUndefined();
    });

    it('should work with a modal heading', async () => {
      element.heading = 'A New Heading!';
      await testWindow.flush();
      expect(element.textContent.trim()).toMatch('A New Heading!');
    });

  });
});
