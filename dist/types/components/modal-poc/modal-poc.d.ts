import '../../stencil.core';
export declare class ModalPoc {
    examples: string[];
    modalEl: HTMLElement;
    heading: string;
    visible: boolean;
    showOptions: boolean;
    open(): void;
    close(): void;
    private handleCancelClick;
    private showOptionsHandler;
    render(): JSX.Element;
}
