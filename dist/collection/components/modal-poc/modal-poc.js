export class ModalPoc {
    constructor() {
        this.examples = ["State Example 1", "State Example 2"];
        this.showOptions = false;
        this.handleCancelClick = () => {
            this.visible = false;
        };
        this.showOptionsHandler = () => {
            this.showOptions = !this.showOptions;
        };
    }
    open() {
        this.visible = true;
    }
    close() {
        this.visible = false;
        this.showOptions = false;
    }
    render() {
        let options = null;
        if (this.showOptions) {
            options = (this.examples.map(exp => (h("p", null, exp))));
        }
        return (h("div", { class: this.visible ? "wrapper visible" : "wrapper" },
            h("div", { class: "modal" },
                h("h2", { class: "modal__title" }, this.heading),
                h("div", { class: "modal__content" },
                    h("slot", { name: "content" }),
                    h("span", { onClick: this.showOptionsHandler },
                        this.showOptions ? "Hide" : "Show",
                        " Options"),
                    options),
                h("div", { class: "modal__footer" },
                    h("slot", { name: "footer" })),
                h("button", { type: "button", class: "modal__close", onClick: this.handleCancelClick }, "X"))));
    }
    static get is() { return "modal-poc"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "close": {
            "method": true
        },
        "heading": {
            "type": String,
            "attr": "heading"
        },
        "modalEl": {
            "elementRef": true
        },
        "open": {
            "method": true
        },
        "showOptions": {
            "state": true
        },
        "visible": {
            "type": Boolean,
            "attr": "visible",
            "reflectToAttr": true,
            "mutable": true
        }
    }; }
    static get style() { return "/**style-placeholder:modal-poc:**/"; }
}
