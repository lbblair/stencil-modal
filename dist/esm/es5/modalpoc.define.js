// modalpoc: Custom Elements Define Library, ES Module/ES5 Target
import { defineCustomElement } from './modalpoc.core.js';
import {
  ModalPoc
} from './modalpoc.components.js';

export function defineCustomElements(window, opts) {
  defineCustomElement(window, [
    ModalPoc
  ], opts);
}